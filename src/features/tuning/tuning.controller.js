class tuningCtrl { 
  constructor(tuningService,$state) {
    let vm = this
    vm.getItems = getItems
    vm.toggleSearch = toggleSearch
    vm.showModal = showModal
    vm.limitStrLength = limitStrLength
    vm.showConfirm = showConfirm

    function limitStrLength(str) {
      return str.substr(0, 100)
    }

    function getItems(type) {
      vm.active = type
      tuningService.getItems(type, res => {
        vm.items = res
      })
    }

    function toggleSearch() {
      vm.keyword = ''
      vm.filter = !vm.filter
      if(!vm.filter) getItems(vm.active)
    }

    function showModal(obj) {
      obj.type = vm.active
      tuningService.showModal(obj)
    }

    function showConfirm(obj) {
      tuningService.showConfirm(obj)
    }

    getItems('body_design')
  }
}

tuningCtrl.$inject = ['tuningService','$state']

export default angular.module('tuning.controller', [])
  .controller('tuningCtrl', tuningCtrl)
  .name