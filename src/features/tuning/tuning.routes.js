tuningRoutes.$inject = ['$stateProvider']

function tuningRoutes($stateProvider) {
  $stateProvider
    .state('tuning', {
      url: '/tuning',
      views: {
        'content': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('./tuning.html')))
            })
          }],
          controller: 'tuningCtrl',
          controllerAs: 'tuning'
        },
        'menu': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('../menu/menu.html')))
            })
          }],
          controller: 'menuCtrl',
          controllerAs: 'menu'
        },
        'modals': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('../modals/modals.html')))
            })
          }],
          controller: 'modalsCtrl',
          controllerAs: 'modals'
        }
      },
      resolve: {
        'currentAuth': ['authService', function(authService) {
          return authService.check()
        }],
        'loadUsersController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('./index')
              $ocLazyLoad.load({name: 'tuning'})
              resolve(module.controller)
            })
          })
        }],
        'loadMenuController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('../menu/index')
              $ocLazyLoad.load({name: 'menu'})
              resolve(module.controller)
            })
          })
        }],
        'loadModalsController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('../modals/index')
              $ocLazyLoad.load({name: 'modals'})
              resolve(module.controller)
            })
          })
        }]
      }
    })
}

export default angular.module('tuning.routing', [])
  .config(tuningRoutes)
  .name