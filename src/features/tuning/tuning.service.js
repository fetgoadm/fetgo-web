import modalsService from '../modals/modals.service'

class tuningService {
  constructor($firebaseArray,modalsService) {
    this.modal = modalsService
    this.array = $firebaseArray
  }

  getItems(type, callback) {
    const ref = firebase.database().ref('car_tuning').child(type).orderByChild('name')
    const data = this.array(ref)
    callback(data)
  }

  showModal(obj) {
    this.modal.showModal(11,obj)
  }
  

  showConfirm(obj) {
    this.modal.showModal(12, obj)
  }
}
  
tuningService.$inject = ['$firebaseArray','modalsService']

export default angular.module('tuning.service', [])
  .service('tuningService', tuningService)
  .name