import tuningService from './tuning.service'
import tuningCtrl from './tuning.controller'

export default angular.module('tuning', [tuningCtrl, tuningService])
  .name