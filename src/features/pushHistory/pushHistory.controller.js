class pushHistoryCtrl { 
  constructor(pushHistoryService,$timeout,$scope) {
    let vm = this
    vm.viewDetails = viewDetails
    vm.toggleFilter = toggleFilter
    vm.resetFilter = resetFilter
    vm.applyFilter = applyFilter
    vm.filters = {
      minDate: null,
      maxDate: null,
      keyword: null,
      limit: 15,
      active: false
    }

    function getNotificationHistory(opts) {
      pushHistoryService.getNotificationHistory(opts, res => {
        vm.notifications = res
        $timeout(() => {
          $scope.$digest()
        })
      })
    }

    function viewDetails(push) {
      pushHistoryService.viewDetails(push)
    }

    function toggleFilter() {
      vm.error = false
      vm.filters.minDate = null
      vm.filters.maxDate = null
      vm.filters.active = !vm.filters.active
    }

    function applyFilter() {
      if(vm.filters.minDate && vm.filters.maxDate) {
        vm.error = false
        getNotificationHistory(vm.filters)
      }
      else vm.error = true
    }

    function resetFilter() {
      vm.error = false
      vm.filters.minDate = null
      vm.filters.maxDate = null
      vm.filters.active = !vm.filters.active
      getNotificationHistory(vm.filters)
    }

    function getToday() {
      const date = new Date()
      let dd = date.getDate()
      if(dd < 10) {dd = '0' + dd}
      let mm = date.getMonth()+1
      if(mm < 10) {mm = '0' + mm}
      let yyyy = date.getFullYear()
      vm.today = yyyy + '-' + mm + '-' + dd
    }

    const element = document.querySelector('.scrollable')

    element.onscroll = ev => {
      if (element.scrollHeight - element.scrollTop === element.clientHeight) {
        vm.filters.limit += 15
        getNotificationHistory(vm.filters)
      }
    }

    getNotificationHistory(vm.filters)
    getToday()
  }
}

pushHistoryCtrl.$inject = ['pushHistoryService','$timeout','$scope']

export default angular.module('pushHistory.controller', [])
  .controller('pushHistoryCtrl', pushHistoryCtrl)
  .name