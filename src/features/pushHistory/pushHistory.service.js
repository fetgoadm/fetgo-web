import modalsService from '../modals/modals.service'
import homeService from '../home/home.service'

export default class pushHistoryService {
  constructor($rootScope,modalsService,homeService) {
    this.home = homeService
    this.modal = modalsService
    this.root = $rootScope
  }

  getNotificationHistory(options,callback) {
    const ref = options.active
      ? firebase.database().ref('notification_history').orderByChild('date').startAt(options.minDate).endAt(options.maxDate)
      : firebase.database().ref('notification_history').limitToLast(options.limit)

    ref.on('value', snap => {
      let data = snap.val()
      let formattedData = []
      for(let key in data) {
        let push = data[key]
        push.temp = {
          date: this.home.formatDate(push.date),
          time: this.home.formatTime(push.time)
        }
        formattedData.push(push)
      }
      callback(formattedData)
    })
  }

  viewDetails(push) {
    this.modal.showModal(7,push)
  }
}
  
pushHistoryService.$inject = ['$rootScope','modalsService','homeService']