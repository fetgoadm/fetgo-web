import pushHistoryCtrl from './pushHistory.controller'
import pushHistoryService from './pushHistory.service'
import homeService from '../home/home.service' 

export default angular.module('pushHistory', [pushHistoryCtrl,homeService])
  .service('pushHistoryService', pushHistoryService)
  .name