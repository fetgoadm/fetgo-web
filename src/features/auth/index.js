
import authService from './auth.service'

export default angular.module('auth', [])
  .service('authService', authService)
  .name