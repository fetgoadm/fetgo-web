
export default class authService {
  constructor($firebaseAuth,$state,$rootScope) {
    this.auth = $firebaseAuth
    this.state = $state
    this.root = $rootScope
  }

  async signIn (email,password) {
    try {
      let res = await this.auth().$signInWithEmailAndPassword(email,password)
      let ref = firebase.database().ref('users').child(res.uid)
      let snap = await ref.once('value')
      let data = snap.val()
      if(data.blocked || data.user_type === 'ut-003') {
        this.signOut()
        return {success:false,msg:`There's been a problem authenticating your user`}
      }
      else {
        return {...res,success:true}
      }
    }
    catch(e) {
      return {success:false,msg:`There's been a problem authenticating your user`}
    }
  }

  signOut () {
    this.auth().$signOut()
    this.root.currentUser ? delete this.root.currentUser : null
  }

  async check() {
    try {
      let response = await this.auth().$requireSignIn()
      return response
    }
    catch(e) {
      console.log(e)
      this.state.go('login')
    }
  }

}

authService.$inject = ['$firebaseAuth','$state','$rootScope']