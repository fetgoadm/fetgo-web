const ngFileUpload = require('ng-file-upload')

class modalsCtrl { 
  constructor(modalsService,$scope,$timeout,$window,$rootScope,$state) {
    let vm = this
    vm.resetModal = resetModal
    vm.printOrder = printOrder
    vm.openPic = openPic
    vm.attendTicket = attendTicket
    vm.resetPass = resetPass
    vm.createUser = createUser
    vm.blockUser = blockUser
    vm.updateSale = updateSale
    vm.sendNotification = sendNotification
    vm.uploadFile = uploadFile
    vm.removeFile = removeFile
    vm.uploadSingleFile = uploadSingleFile
    vm.editCarTuning = editCarTuning
    vm.removeCarTuningImg = removeCarTuningImg
    vm.uploadNewTuningImg = uploadNewTuningImg
    vm.removeCarTuning = removeCarTuning
    vm.updateUserPermissions = updateUserPermissions
    vm.saveInsurance = saveInsurance
    vm.deleteInsurance = deleteInsurance
    vm.closeRoom = closeRoom
    
    function openPic(url) {
      /* Check if document was uploaded as an image file or as PDF.
          Then it either shows the image in the app or downloads the PDF file */
      const regex = /.*\.(png|gif|jpg|jpeg|JPG|GIF|JPEG|PNG).*/
      url.match(regex) ? vm.activePic = url : $window.open(url)
      updateView()
    }

    function toast(text) {
      updateView()
      modalsService.toast(text)
    }

    function resetModal() {
      modalsService.resetModal()
    }

    function printOrder() {
      modalsService.printOrder()
    }

    async function attendTicket(ticket,status) {
      let res = await modalsService.updateStatus(ticket,status)
      if(res.success) {
        ticket.category === 'chat' && status === 1 && $state.go('chat')
        toast(`Ticket No. ${ticket.ref} status has been changed`)
        resetModal()
      }
      else {
        toast('An error has occured. Please try again.')
        console.log(res.e)
      }
    }

    async function createUser(form) {
      if(form.$valid) {
        loading(true)
        const checks = document.querySelectorAll('.perms')
        const obj = checkboxTransform(checks)
        const data = {...vm.activeModalData, permissions: obj}
        let res = await modalsService.createUserAuth(data)
        loading(false)
        if(res.success) {
          toast('User has been created. Page will now reload')
          resetModal()
          setTimeout(reload, 3000)
        }
        else {
          toast('User already exists')
        }
      }
    }

    async function resetPass(user) {
      loading(true)
      let res = await modalsService.resetPass(user.email)
      loading(false)
      if(res.success) {
        toast('Email has been sent')
        resetModal()
      }
      else {
        toast('An error has occured. Please try again.')
      }
    }

    async function blockUser(user) {
      loading(true)
      let res = await modalsService.blockUser(user)
      loading(false)
      if(res.success) {
        toast('User status has been changed')
      }
      else {
        toast('An error has occured. Please try again.')
      }
      updateView()
    }

    async function updateSale(status,sale) {
      loading(true)
      let res = await modalsService.updateSale(status,sale)
      loading(false)
      if(res.success) {
        toast('Car sale status has been changed')
        resetModal()
      }
      else {
        toast('An error has occured. Please try again.')
      }
      updateView()
    }

    async function editCarTuning(form) {
      if(form.$valid) {
        loading(true)
        let res = await modalsService.editCarTuning()
        loading(false)
        if(res.success) {
          toast('Item has been saved')
          resetModal()
        }
        else {
          toast('An error has occured. Please try again.')
          console.log(res.e)
        }
        updateView()
      }
    }

    async function sendNotification(user,content) {
      loading(true)
      const userData = {
        id: user.$id,
        name: user.name || user.displayName.split(' ').shift(),
        lastName: user.lastName || user.displayName.split(' ').pop(),
        fullName: user.displayName || `${user.name} ${user.lastName}`,
        email: user.email || 'N/A'
      }
      let res = await modalsService.sendNotification(false,userData,content)
      loading(false)
      if(res.success) {
        toast('Push notification sent')
        resetModal()
      }
      else {
        toast('An error has occured. Please try again')
      }
      updateView()
    }

    function updateView() {
      $timeout(() => {
        $scope.$digest()
      })
    }

    function loading(status) {
      let btn = angular.element(document.querySelector('#userBtn'))
      let spinner = angular.element(document.querySelector('.modalOverlay'))
      if(status) {
        btn.addClass('disabled')
        spinner.addClass('loading')
      }
      else {
        btn.removeClass('disabled')
        spinner.removeClass('loading')
      }
    }

    function reload() {
      window.location.reload()
    }

    async function uploadFile(file,index) {
      let res = await modalsService.uploadFile(file,index)
      if(res.success) {
        updateView()
      }
    }

    function removeFile(index) {
      modalsService.removeFile(index)
    }

    async function uploadSingleFile() {
      let res = await modalsService.uploadSingleFile()
      if(res.success) {
        // Set timeout to 2 secs to solve view update issue
        $timeout(() => {
          $scope.$digest()
        }, 2000)
      }
      else {
        console.log(res.e)
        toast('An error has occured. Please try again.')
      }
    }

    async function removeCarTuningImg(index) {
      let res = await modalsService.removeCarTuningImg(index)
      if(res.success) {
        updateView()
      }
    }

    async function uploadNewTuningImg() {
      let res = await modalsService.uploadNewTuningImg()
      if(res.success) {
        updateView()
      }
      else {
        toast('An error has occured. Please try again.')
      }
    }

    async function removeCarTuning() {
      let res = await modalsService.removeCarTuning()
      if(res.success) {
        toast(`Item has been removed.`)
        resetModal()
      }
      else {
        toast('An error has occured. Please try again.')
      }
    }

    async function updateUserPermissions() {
      loading(true)
      const checks = document.querySelectorAll('.perms')
      const obj = checkboxTransform(checks)
      let res = await modalsService.updateUserPermissions(obj)
      loading(false)
      if(res.success) {
        toast('Permissions changed successfully')
        resetModal()
      }
      else {
        toast('An error has occured. Please try again')
      }
    }

    function checkboxTransform(checks) {
      const map = new Map()
      checks.forEach(item => {
        if(item.checked) {
          map.set(item.id, vm.permissions[item.id])
        }
      })
      const obj = Array.from(map).reduce((obj, [key, value]) => (
        Object.assign(obj, { [key]: value })
      ), {})
      return obj
    }

    async function getPermissions() {
      if($state.current.name === 'users') {
        let res = await modalsService.getPermissions()
        vm.permissions = res.data
      }
    }

    async function saveInsurance(form) {
      if(form.$valid) {
        loading(true)
        let res = await modalsService.saveInsurance()
        loading(false)
        if(res.success) {
          toast('Item has been saved')
          resetModal()
        }
        else {
          toast('An error has occured. Please try again.')
          console.log(res.e)
        }
        updateView()
      }
    }

    
    async function deleteInsurance() {
      loading(true)
      let res = await modalsService.deleteInsurance()
      loading(false)
      if(res.success) {
        toast('Item has been deleted')
        resetModal()
      }
      else {
        toast('An error has occured. Please try again.')
        console.log(res.e)
      }
      updateView()
    }

    async function closeRoom() {
      const res = await modalsService.closeRoom()
      if(res.success) {
        toast('Chat room has been closed')
        resetModal()
      }
      else {
        toast('An error has occured. Please try again.')
        console.log(res.e)
      }
    }

    getPermissions()
  } 
}

modalsCtrl.$inject = ['modalsService','$scope','$timeout','$window','$rootScope','$state']

export default angular.module('modals.controller', [ngFileUpload])
  .controller('modalsCtrl', modalsCtrl)
  .name