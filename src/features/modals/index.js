import modalsService from './modals.service'
import modalsCtrl from './modals.controller'

export default angular.module('modals', [modalsCtrl])
  .service('modalsService', modalsService)
  .name