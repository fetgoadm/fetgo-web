import moment from 'moment-timezone'

export default class modalsService {
  constructor($rootScope,$window,$timeout,$firebaseAuth,$firebaseObject,$firebaseStorage,$http,$state) {
    this.auth = $firebaseAuth
    this.root = $rootScope
    this.window = $window
    this.obj = $firebaseObject
    this.http = $http
    this.timeout = $timeout
    this.storage = $firebaseStorage
    this.state = $state
  }

  attachments = []
  permissions = {}

  // Show ticket modal
  showModal(type,obj) {
    console.log(obj)
    document.getElementsByTagName("BODY")[0].style.overflow = 'hidden'
    this.root.modalType = type
    if(!this.root.activeModalData) {
      this.root.activeModalData = obj ? {...obj} : {} // Set empty object for create user modals
    }
    if(this.root.modalType === 8) {
      if(!this.root.activeModalData.files) {
        this.root.activeModalData.files = [null]
      }
    }
  }

  // Reset modal
  resetModal() {
    document.getElementsByTagName("BODY")[0].style.overflow = 'auto'
    if(this.root.modalType !== 8) delete this.root.activeModalData
    delete this.root.modalType
  }

  // Print ticket details
  printOrder() {
    this.window.print()
  }

  // Update ticket status
  async updateStatus(ticket,status,callback) {
    const orders = firebase.database().ref('orders')
    const historyOrders = firebase.database().ref('history_orders')
    let newTicket = {...ticket}
    delete newTicket.$$hashKey
    delete newTicket.temp
    if(status === 0) {
      newTicket = {
        ...newTicket,
        status: 'rejected',
        attended_by: this.root.currentUser.uid,
        attended_by_name: `${this.root.currentUser.data.name} ${this.root.currentUser.data.lastName}`,
        attended_date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
        attended_time: moment().tz('asia/dubai').format('hh:mm:ss a'),
        completed_date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
        completed_time: moment().tz('asia/dubai').format('hh:mm:ss a')
      }

      try {
        await orders.child(ticket.temp.userId).child(ticket.id).remove()
        await historyOrders.child(newTicket.id).set(newTicket)
        return {success:true}
      }
      catch(e) {
        return {success:false}
      }
    }
    if(status === 1) {
      newTicket = {
        ...newTicket,
        status: 'approved',
        attended_date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
        attended_time: moment().tz('asia/dubai').format('hh:mm:ss a'),
        attended_by: this.root.currentUser.uid,
        attended_by_name: `${this.root.currentUser.data.name} ${this.root.currentUser.data.lastName}`
      }
      try {
        Promise.all([
          await orders.child(ticket.temp.userId).child(ticket.id).update(newTicket),
          ticket.category === 'chat' && await this.createNewChatRoom()
        ])
        return {success:true}
      }
      catch(e) {
        return {success:false, e}
      }
    }
    if(status === 2) {
      newTicket = {
        ...newTicket,
        status: 'completed',
        completed_date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
        completed_time: moment().tz('asia/dubai').format('hh:mm:ss a')
      }
      try {
        await orders.child(ticket.temp.userId).child(ticket.id).remove()
        await historyOrders.child(newTicket.id).set(newTicket)
        return {success:true}
      }
      catch(e) {
        return {success:false}
      }
    }
  }

  // Update sale ad status
  async updateSale(status,sale) {
    let obj = {...sale}
    obj.status = status === true ? 'approved' : 'rejected'
    delete obj.temp
    delete obj.$$hashKey
    const ref = firebase.database().ref('sells').child(obj.id)
    try {
      await ref.update(obj)
      return {success:true}
    }
    catch(e) {
      return {success:false,e}
    }
  }
  
  // Send notification to users
  async sendNotification(allUsers,userData,content) {
    const attachments = this.attachments.length ? this.attachments : null
    Array.isArray(userData)
      ? null
      : userData = [userData]
    const uidList = userData
      .map(user => {
        return user.id
      })
      .join(',')

    try {
      let res = await this.http({
        method: 'POST',
        url: 'https://us-central1-fetgo-203d8.cloudfunctions.net/sendPushNotification',
        data: {
          uids: uidList,
          message: content,
          date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
          time: moment().tz('asia/dubai').format('hh:mm:ss a'),
          attachments
        },
        headers: {
          'Content-Type': 'application/json'
        }
      })

      const ref = firebase.database().ref('notification_history')
      const key = res.data.uid
      const obj = {
        content: content,
        users: userData,
        sent_by: this.root.currentUser.uid,
        sent_by_name: `${this.root.currentUser.data.name} ${this.root.currentUser.data.lastName}`,
        date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
        time: moment().tz('asia/dubai').format('hh:mm:ss a'),
        id: key,
        allUsers: allUsers,
        attachments
      }
      try {
        await ref.child(key).set(obj)
        return {success:true}
      }
      catch(e) {
        console.log(e)
        return {success:false}
      }
    }
    catch(e) {
      return {success:false}
    }
  }

  // Create user in Firebase Auth
  async createUserAuth(obj) {
    try {
      let email = String(this.root.currentUser.data.email)
      let pass = String(this.root.currentUser.data.pass)
      let res = await this.auth().$createUserWithEmailAndPassword(obj.email,obj.pass)
      await this.auth().$signOut()
      await this.auth().$signInWithEmailAndPassword(email,pass)
      if(res.uid) {
        try {
          let res2 = await this.createUserDB(obj,res.uid)
          return {success:true,res2}
        }
        catch(e) {
          return {success:false,e}
        }
      }
    }
    catch(e) {
      return {success:false,e}
    }
  }

  // Create user collection in Firebase Database
  async createUserDB(obj,key) {
    const ref = firebase.database().ref('users').child(key)
    let user = {
      ...obj,
      id: key
    }
    delete user.pass2
    if(user.user_type === 'ut-001') user.user_type_name = 'Admin'
    if(user.user_type === 'ut-002') user.user_type_name = 'Agent'
    if(user.user_type === 'ut-003') user.user_type_name = 'App User'
    try {
      await ref.set(user)
      return {success:true}
    }
    catch(e) {
      return {success:false,e}
    }
  }

  // Send password reset email to user
  async resetPass(email) {
    try {
      await this.auth().$sendPasswordResetEmail(email)
      return {success:true}
    }
    catch(e) {
      return {success:false,e}
    }
  }

  // Set user status to blocked / unblocked
  async blockUser(user) {
    let obj = {...user}
    obj.blocked ? obj.blocked = !obj.blocked : obj.blocked = true
    obj.id = obj.$id
    delete obj.$$hashKey
    delete obj.$id
    delete obj.$priority
    try {
      let ref = firebase.database().ref('users').child(obj.id)
      await ref.update(obj)
      this.resetModal()
      return {success:true}
    }
    catch(e) {
      return {success:false,e}
    }
  }

  // Toast alert handler
  toast(text) {
    this.root.alert = true
    this.root.toastText = text
    this.timeout(() => {
      this.root.alert = false
    },3000)
  }

  // File upload function for notification attachments
  async uploadFile(file,index) {
    // Get file array from form
    let files = this.root.activeModalData.files

    // Add a new input type file if limit hasn't been exceeded
    if(files.length < 5) files.push(null)

    // Get file extension
    const fileType = file.type.split('/').pop()

    // Prepare ref for Storage Put
    const today = moment().format('YYYY-MM-DD')
    const ref = firebase.database().ref('notification_history')
    const key = ref.push().key 
    const storageRef = firebase.storage().ref('notifications').child(today).child(`${key}.${fileType}`)
    const storage = this.storage(storageRef)
    let task = storage.$put(file)

    // Bind upload progress to form item
    task.$progress(snap => {
      let progress = (snap.bytesTransferred / snap.totalBytes) * 100
      this.root.activeModalData.files[index].progress = progress
    })

    // Push file storage URL to attachments array
    await task.$complete(snap => {
      this.attachments.push(snap.downloadURL)
    })
    return {success:true}
  }

  // Remove file from attachments array
  removeFile(index) {
    this.root.activeModalData.files[index] = null
    this.attachments[index] = null
  }

  // Return Firebase Collection array name based on current UI Router state
  getCollectionName() {
    const state = this.state.current.name
    switch(state) {
      case 'tuning':
        return 'car_tuning'
        break
      case 'rental':
        return 'car_rental'
        break
    }
  }
  
  // Upload image and push to item image array in database
  async uploadSingleFile() {
    // Get collection name (tuning or rental)
    const collectionName = this.getCollectionName()

    const file = this.root.activeModalData.fileToUpload

    // Get file extension
    const fileType = file.type.split('/').pop()

    // Get item category for ref building purposes and build file name for Storage
    const type = this.root.activeModalData.category
    const dbRef = firebase.database().ref(collectionName).child(type)
    const key = this.root.activeModalData.id
      ? this.root.activeModalData.id
      : dbRef.push().key
    let storageHash = dbRef.push().key
    storageHash = storageHash.substring(storageHash.length - 5)
    const storageRef = firebase.storage().ref(collectionName).child(type).child(key).child(`${key}-${storageHash}.${fileType}`)
    const storage = this.storage(storageRef)
    const task = storage.$put(file)
    try {
      await task.$complete(async snap => {
        const url = snap.downloadURL
        // Save image URL in item collection in database
        const length = this.root.activeModalData.images.length
        await dbRef.child(key).child('images').child(length).set(url)

        this.root.activeModalData.images.push(url)

        // Delete file object
        delete this.root.activeModalData.fileToUpload
      })
      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }

  // Add / Edit item in tuning or rental in database
  async editCarTuning() {
    // Get collection name
    const collectionName = this.getCollectionName()

    // Get data from modal and prepare for upload
    const modal = {...this.root.activeModalData}
    const item = {
      category: modal.category,
      description: modal.description || null,
      id: modal.id,
      images: modal.images,
      name: modal.name || null,
      prices: modal.prices || null,
      price: modal.price || null,
      make: modal.make || null,
      year: modal.year || null
    }
    const ref = firebase.database().ref(collectionName).child(item.category).child(item.id)
    try {
      await ref.set(item)
      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }

  // Remove rental or tuning item image from database
  async removeCarTuningImg(index) {
    const collectionName = this.getCollectionName()
    const { id, category, images } = this.root.activeModalData
    images.splice(index, 1)
    const ref = firebase.database().ref(collectionName).child(category).child(id).child('images')
    await ref.set(images)
    return {success:true}
  }

  // Uploads image to Storage for new items that haven't been saved in Database yet
  async uploadNewTuningImg() {
    const collectionName = this.getCollectionName()

    // Get data from modal form object
    let { id, category, images, fileToUpload } = this.root.activeModalData

    // Get file type
    const fileType = fileToUpload.type.split('/').pop()

    //Set database reference for retrieving uid
    const dbRef = firebase.database().ref(collectionName)

    // Generate uid if needed (first image upload)
    if(!id) {
      id = dbRef.push().key
      this.root.activeModalData.id = id
    }

    // Generate hash from uid for Firebase Storage file name
    let storageHash = dbRef.push().key
    storageHash = storageHash.substring(storageHash.length - 5)

    //Set Firebase Storage ref depending on whether category has already been selected
    let storageRef = firebase.storage().ref(collectionName).child(`${id}-${storageHash}.${fileType}`)
    if(category) {
      storageRef = firebase.storage().ref(collectionName).child(category).child(id).child(`${id}-${storageHash}.${fileType}`)
    }

    // Setup AngularFire
    const storage = this.storage(storageRef)
    const task = storage.$put(fileToUpload)
    try {
      await task.$complete(async snap => {
        // Check if local images array exists and create it if needed
        if(!images) {
          images = []
        }

        // Push image URL to local array
        images.push(snap.downloadURL)
        this.root.activeModalData.images = images

        // Delete file object
        delete this.root.activeModalData.fileToUpload
      })
      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }

  // Remove rental or tuning item from database
  async removeCarTuning() {
    const collectionName = this.getCollectionName()

    const { id, category } = this.root.activeModalData
    const ref = firebase.database().ref(collectionName).child(category).child(id)
    try {
      await ref.remove()
      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }

  async getPermissions() {
    if(Object.keys(this.permissions).length) {
      return {success:true, data: this.permissions}
    }
    else {
      const ref = firebase.database().ref('categories')
      const snap = await ref.once('value')
      this.permissions = {...snap.val()}
      return {success:true, data: this.permissions}
    }
  }

  async updateUserPermissions(perms) {
    const { id } = this.root.activeModalData
    const ref = firebase.database().ref('users').child(id).child('permissions')
    try {
      await ref.set(perms)
      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }

  async saveInsurance() {
    const { query, activeModalData } = this.root
    let key = activeModalData.company_name.split(' ').join('_')
    const ref = firebase.database().ref(query.type).child(query.service).child(key)
    try {
      await ref.set(activeModalData)
      return {success:true}
    }
    catch (e) {
      return {success:false, e}
    }
  }

  async deleteInsurance() {
    const { query, activeModalData } = this.root
    const ref = firebase.database().ref(query.type).child(query.service).child(activeModalData.id)
    try {
      await ref.remove()
      return {success:true}
    }
    catch (e) {
      return {success:false, e}
    }
  }

  async createNewChatRoom() {
    const ticket = {...this.root.activeModalData}
    const currentUser = {...this.root.currentUser}
    const client = {
      fcm_token: ticket.fcm_token,
      email: ticket.email,
      uid: ticket.temp.userId,
      name: ticket.name || null,
      lastName: ticket.lastName || null,
      displayName: ticket.displayName || null,
      phone: ticket.phone
    }
    const operator = {
      uid: currentUser.uid,
      name: currentUser.data.name,
      lastName: currentUser.data.lastName,
      email: currentUser.data.email
    }
    const chatRef = firebase.database().ref('chat')
    const key = chatRef.push().key
    const clientRef = firebase.database().ref('users').child(client.uid).child('rooms').child(key)
    const operatorRef = firebase.database().ref('users').child(operator.uid).child('rooms').child(key)
    const chatRoom = {
      about: ticket.about,
      uid: key,
      client,
      operator,
      o_flag: true,
      ticketId: ticket.id || null
    }
    const userRoom = {
      about: ticket.about,
      date: ticket.date,
      time: moment().tz('asia/dubai').format('hh:mm:ss a'),
      uid: key,
      client: `${client.name} ${client.lastName}` || client.displayName,
      clientuid: client.uid,
      operator: `${operator.name} ${operator.lastName}`,
      operatoruid: operator.uid
    }

    try {
      await Promise.all([
        chatRef.child(key).set(chatRoom),
        clientRef.set(userRoom),
        operatorRef.set(userRoom)
      ])

      this.root.roomId = key

      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }
  
  async closeRoom() {
    const { uid: roomId, client: { uid: clientId}, operator: { uid: agentId } } = this.root.activeModalData
    const clientRef = firebase.database().ref('users').child(clientId).child('rooms').child(roomId)
    const agentRef = firebase.database().ref('users').child(agentId).child('rooms').child(roomId)
    const roomRef = firebase.database().ref('chat').child(roomId)
    try {
      Promise.all([
        await clientRef.remove(),
        await agentRef.remove(),
        await roomRef.update({o_flag: false})
      ])
      
      return {success:true}
    }
    catch(e) {
      return {success:false, e}
    }
  }
}

modalsService.$inject = ['$rootScope','$window','$timeout','$firebaseAuth','$firebaseObject','$firebaseStorage','$http','$state']