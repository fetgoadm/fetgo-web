homeRoutes.$inject = ['$stateProvider']

function homeRoutes($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/home',
      views: {
        'content': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('./home.html')))
            })
          }],
          controller: 'homeCtrl',
          controllerAs: 'home'
        },
        'menu': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('../menu/menu.html')))
            })
          }],
          controller: 'menuCtrl',
          controllerAs: 'menu'
        },
        'modals': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('../modals/modals.html')))
            })
          }],
          controller: 'modalsCtrl',
          controllerAs: 'modals'
        }
      },
      resolve: {
        'currentAuth': ['authService', authService => {
          return authService.check()
        }],
        'loadHomeController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('./index')
              $ocLazyLoad.load({name: 'home'})
              resolve(module.controller)
            })
          })
        }],
        'loadMenuController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('../menu/index')
              $ocLazyLoad.load({name: 'menu'})
              resolve(module.controller)
            })
          })
        }],
        'loadModalsController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('../modals/index')
              $ocLazyLoad.load({name: 'modals'})
              resolve(module.controller)
            })
          })
        }]
      }
    })
}

export default angular.module('home.routing', [])
  .config(homeRoutes)
  .name