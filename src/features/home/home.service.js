import moment from 'moment-timezone'
import modalsService from '../modals/modals.service'
import soundFile from '../../assets/sounds/notification.mp3'
import menuService from '../menu/menu.service'

class homeService {
  constructor($rootScope,modalsService,menuService) {
    this.root = $rootScope
    this.modal = modalsService
    this.menu = menuService
  }

  // Create order tickets array from database
  getWaitingTickets = async callback => {
    const userData = await this.menu.getUser()
    const ref = firebase.database().ref('orders')
    ref.on('value', snap => {
      let tickets = [] // Waiting ticket array
      let myTickets = [] // My approved tickets array
      let oldLength = tickets.length
      let data = snap.val()
      for(let user in data) {
        let userUid = user
        for(let order in data[user]) {
          // Push tickets to array 
          let ticket = this.formatTicketData(data[user][order], userUid)
          const { user_type, permissions } = userData.data
          !data[user][order].attended_by
            ? (() => {
              if(user_type === 'ut-001' || permissions[ticket.category] || ticket.about && permissions[ticket.about]) {
                let oldLength = tickets.length
                tickets.push(ticket)
              }
            })()
            : null
          
          /* Feed global ticket lists for Admin type users
            and user specific lists for Agent type users */
          userData.data.user_type === 'ut-001'
            ? data[user][order].attended_by
              ? myTickets.push(ticket)
              : null
            : data[user][order].attended_by === userData.uid
              ? myTickets.push(ticket)
              : null
        }
      }
      oldLength < tickets.length ? new Audio(soundFile).play() : null
      callback({tickets,myTickets})
    })
  }

  getRejectedTickets = async callback => {
    const userData = await this.menu.getUser()
    const ref = firebase.database().ref('history_orders').orderByChild('attended_date').equalTo(moment().format('YYYY-MM-DD'))
    ref.on('value', snap => {
      let rejectedTickets = []
      let data = snap.val()
      for(let order in data) {
        /* Feed global ticket lists for Admin type users
            and user specific lists for Agent type users */
        data[order].status == 'rejected'
          ? userData.data.user_type === 'ut-001'
            ? (() => {
              let ticket = this.formatTicketData(data[order])
              rejectedTickets.push(ticket)
            })()
            : data[order].attended_by === userData.uid
              ? (() => {
                let ticket = this.formatTicketData(data[order])
                rejectedTickets.push(ticket)
              })()
              : null
          : null
      }
      callback({rejectedTickets})
    })
  }

  // Format ticket information
  formatTicketData = (ticket,userUid) => {
    ticket.userId = userUid
    // temp prop created for viewing purposes
    ticket.temp = {
      title: formatTitle(ticket.category),
      userId: userUid,
      timeAgo: this.formatTimeAgo(ticket.date,ticket.time),
      date: this.formatDate(ticket.date),
      time: this.formatTime(ticket.time),
      attended_date: this.formatDate(ticket.attended_date),
      attended_time: this.formatTime(ticket.attended_time),
      completed_date: ticket.completed_date ? this.formatDate(ticket.completed_date) : null,
      completed_time: ticket.completed_time ? this.formatTime(ticket.completed_time) : null,
      attended_difference: this.dateTimeDiff(ticket.date,ticket.time,ticket.attended_date,ticket.attended_time)
    }
    switch(ticket.category) {
      case 'rentcar':
        ticket.temp.serviceTime = this.formatServiceTime(ticket.category,ticket.rentcar.months,ticket.rentcar.weeks,ticket.rentcar.days)
        break
      case 'carcargo': 
        if(ticket.mode === 'oneWay') {
          ticket.temp.mode = 'One Way'
          ticket.temp.serviceTime = this.formatDate(ticket.firstDate)
        }
        if(ticket.mode === 'twoWay') {
          ticket.temp.mode = 'Two Way'
          ticket.temp.serviceTime = `${this.formatDate(ticket.firstDate)} - ${this.formatDate(ticket.secondDate)}`
        }
        break
      case 'carinsurance':
        ticket.temp.yearVehicleRegistration = ticket.yearVehicleRegistration ? moment(ticket.yearVehicleRegistration, 'YYYY-MM-DD').format('L') : null
        ticket.temp.policyExpireDate = ticket.temp.policyExpireDate ? moment(ticket.policyExpireDate, 'YYYY-MM-DD').format('L') : null
        ticket.temp.dateBirth = ticket.dateBirth ? moment(ticket.dateBirth, 'YYYY-MM-DD').format('L') : null
        break
      case 'Luxurious':
        ticket.dateSelected ? ticket.temp.dateSelected = moment(ticket.dateSelected, 'YYYY-MM-DD').format('L') : null
        if(ticket.serviceModule === 'CHOPPER') ticket.temp.serviceModule = 'Chopper'
        if(ticket.serviceModule === 'DESIGNATED DRIVER') ticket.temp.serviceModule = 'Designated Driver'
        if(ticket.serviceModule === 'PRIVATEJET') {
          ticket.temp.serviceModule = 'Private Jet'
          ticket.selected ? ticket.temp.selected = moment(ticket.selected, 'YYYY-MM-DD').format('L') : null
          ticket.selectedtwo ? ticket.temp.selectedtwo = moment(ticket.selectedtwo, 'YYYY-MM-DD').format('L') : null
        }
        break
      case 'cartuning':
        ticket.temp.date = moment(ticket.date, 'YYYY-MM-DD').format('L')
        break
      case 'chat':
        ticket.temp.about = formatTitle(ticket.about)
        ticket.temp.date = moment(ticket.date, 'YYYY-MM-DD').format('L')
        break
    }
    return ticket
  }

  formatDate = date => {
    return date ? moment(date).format('L') : null
  }

  formatTime = time => {
    return time ? moment(time,'h:mm:ss a').format('LT') : null
  }

  formatServiceTime = (category,months,weeks,days) => {
    let str = ''
    if(months > 0) months > 1 ? str = `${str} ${months} months` : `${str} ${months} month`
    if(weeks > 0) weeks > 1 ? str = `${str} ${weeks} weeks` : str = `${str} ${weeks} week`
    if(days > 0) days > 1 ? str = `${str} ${days} days` : str = `${str} ${days} day`
    return str
  }

  formatTimeAgo = (date,time) => {
    let dt = date + time
    return moment(dt,'YYYY-MM-DDh:mm:ss a').fromNow()
  }

  dateTimeDiff = (date1,time1,date2,time2) => {
    let dt1 = moment(date1 + time1, 'YYYY-MM-DDh:mm:ss a')
    let dt2 = moment(date2 + time2, 'YYYY-MM-DDh:mm:ss a')
    return moment.duration(dt1.diff(dt2)).humanize()
  }

  showModal = ticket => {
    this.modal.showModal(0,ticket)
  }
}
  
homeService.$inject = ['$rootScope','modalsService','menuService']

export default angular.module('home.service', [])
  .service('homeService', homeService)
  .name

export const formatTitle = (category) => {
  switch(category) {
    case 'rentcar': return 'Car Rental'
    case 'carcargo': return 'Car Cargo'
    case 'carservices': return 'Car Services'
    case 'carsupermarket': return 'Buy Parts'
    case 'carinsurance': return 'Car Insurance'
    case 'Luxurious': return 'Luxurious'
    case 'cartuning': return 'Car Tuning'
    case 'QuickInsurance':  return 'Quick Insurance'
    case 'chat': return 'Live Chat'
    case undefined: return
    default: console.warn('Unknown category: ' + category); break
  }
}