import homeService from './home.service'
import homeCtrl from './home.controller'

export default angular.module('home', [homeCtrl,homeService])
  .name