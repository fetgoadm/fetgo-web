class homeCtrl { 
  constructor(homeService,$scope,$timeout,$window) {
    let vm = this
    vm.loading = true
    vm.viewDetails = viewDetails
    
    function viewDetails(ticket) {
      homeService.showModal(ticket)
    }

    function getTickets() {
      homeService.getWaitingTickets(res => {
        vm.tickets = res.tickets
        vm.myTickets = res.myTickets
        vm.loading = false
        updateView()
      })
    }

    function getRejectedTickets() {
      homeService.getRejectedTickets(res => {
        vm.rejectedTickets = res.rejectedTickets
        updateView()
      })
    }

    function updateView() {
      $timeout(function() {
        $scope.$digest()
      })
    }

    getTickets()
    getRejectedTickets()
  } 
}

homeCtrl.$inject = ['homeService','$scope','$timeout','modalsService']

export default angular.module('home.controller', [])
  .controller('homeCtrl', homeCtrl)
  .name