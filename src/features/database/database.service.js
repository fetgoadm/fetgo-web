class databaseService {
  constructor($rootScope,$http) {
    this.root = $rootScope
    this.http = $http
  }

  async fetchData(query) {
    try {
      const res = await this.http({
        method: 'GET',
        url: 'https://us-central1-fetgo-203d8.cloudfunctions.net/fetchCar',
        params: query
      })
      return {success:true, ...res}
    }
    catch(e) {
      console.log(e)
      return {success:false, e}
    }
  }

  async submitNew(query, type, name) {
    let ref = null
    let obj = null
    if(type === 'make') {
      ref = firebase.database().ref('carsData').child(query.year).child(name)
      obj = {
        make_display: name,
        make_id: name.toLowerCase(),
        models: false,
        registeredFromWeb: true
      }
    }
    if(type === 'model') {
      ref = firebase.database().ref('carsData').child(query.year).child(query.make).child('models').child(name)
      obj = {
        model_name: name,
        model_trims: false,
        model_make_id: query.make.toLowerCase(),
        registeredFromWeb: true
      }
    }
    if(type === 'modelType') {
      ref = firebase.database().ref('carsData').child(query.year).child(query.make).child('models').child(query.model).child('modelTrims').child(name)
      obj = {
        make_display: query.make,
        model_make_display: query.make,
        model_make_id: query.make.toLowerCase(),
        model_trim: name,
        registeredFromWeb: true
      }
    }
    
    try {
      await ref.set(obj)
      return {success:true}
    }
    catch (e) {
      return {success:false, e}
    }
  }

  async deleteItem(query, type, name) {
    let ref = null
    if(type === 'make') {
      ref = firebase.database().ref('carsData').child(query.year).child(name)
    }
    if(type === 'model') {
      ref = firebase.database().ref('carsData').child(query.year).child(query.make).child('models').child(name)
    }
    if(type === 'modelType') {
      ref = firebase.database().ref('carsData').child(query.year).child(query.make).child('models').child(query.model).child('modelTrims').child(name)
    }

    try {
      await ref.remove()
      return {success: true}
    }
    catch (e) {
      return {success:false, e}
    }
  }
}
  
databaseService.$inject = ['$rootScope','$http']

export default angular.module('database.service', [])
  .service('databaseService', databaseService)
  .name