import modalsService from '../modals/modals.service'

class databaseCtrl { 
  constructor(databaseService,$scope,$timeout,modalsService) {
    const vm = this
    vm.query = {
      year: null,
      make: null,
      model: null
    }
    vm.years = getYears()
    vm.fetchData = fetchData
    vm.setYear = setYear
    vm.setMake = setMake
    vm.setModel = setModel
    vm.toggleSearch = toggleSearch
    vm.toggleInput = toggleInput
    vm.submitNew = submitNew
    vm.deleteItem = deleteItem

    function getYears() {
      let years = []
      const date = new Date().getFullYear()
      for(let i = 0; i <= 30; i++) {
        years.push(date - i)
      }
      return years
    }

    async function fetchData() {
      const res = await databaseService.fetchData(vm.query)
      if(res.success) {
        bindData(res.data)
      }
      else {
        modalsService.toast('An error has ocurred')
      }
    }

    function setYear(year) {
      vm.query.year = year
      vm.query.make = null
      vm.query.model = null
      delete vm.makes
      delete vm.models
      delete vm.modelTypes
      fetchData()
    }

    function setMake(make) {
      vm.query.make = make
      vm.query.model = null
      delete vm.models
      delete vm.modelTypes
      fetchData()
    }

    function setModel(model) {
      vm.query.model = model
      fetchData()
    }

    function bindData(data) {
      if(vm.query.year && !vm.query.make && !vm.query.model) {
        vm.makes = Object.values(data)
      }
      else if(vm.query.year && vm.query.make && !vm.query.model) {
        vm.models = Object.values(data.models)
      }
      else if(vm.query.year && vm.query.make && vm.query.model) {
        vm.modelTypes = Object.values(data.modelTrims)
      }
      else {
        console.log('ERROR')
      }
      updateView()
    }

    function toggleSearch(type) {
      switch(type) {
        case 'make':
          vm.makeSearch = !vm.makeSearch
          vm.kwMake = ''
          break
        case 'model':
          vm.modelSearch = !vm.modelSearch
          vm.kwModel = ''
          break
        case 'modelType':
          vm.modelTypeSearch = !vm.modelTypeSearch
          vm.kwModelType = ''
          break
        default:
          vm.makeSearch = !vm.makeSearch
          vm.kwMake = ''
          vm.modelSearch = !vm.modelSearch
          vm.kwModel = ''
          vm.modelTypeSearch = !vm.modelTypeSearch
          vm.kwModelType = ''
          break
      }
    }

    function toggleInput(type) {
      switch(type) {
        case 'make':
          vm.showMakeInput = !vm.showMakeInput
          delete vm.newMake
          break
        case 'model':
          vm.showModelInput = !vm.showModelInput
          delete vm.newModel
          break
        case 'modelType':
          vm.showModelTypeInput = !vm.showModelTypeInput
          delete vm.newModelType
          break
      }
    }

    async function submitNew(type) {
      let name = null
      switch(type) {
        case 'make':
          name = vm.newMake
          break
        case 'model':
          name = vm.newModel
          break
        case 'modelType':
          name = vm.newModelType
          break
      }
      if(name && name.length) {
        const res = await databaseService.submitNew(vm.query,type,name)
        if(res.success) {
          if(type === 'make') {
            vm.query.model = null
            delete vm.models
            delete vm.modelTypes
            delete vm.newMake
          }
          if(type === 'model') {
            delete vm.modelTypes
            delete vm.newModel
          }
          if(type === 'modelType') {
            delete vm.newModelType
          }
          toggleInput(type)
          await fetchData()
          updateView()
          modalsService.toast(`${type} added successfully`)
        }
        else {
          modalsService.toast('An error has ocurred')
          console.log(res.e)
        }
      }
    }

    async function deleteItem(event,type,name) {
      event.stopPropagation()
      const res = await databaseService.deleteItem(vm.query,type,name)
      if(res.success) {
        if(type === 'make') {
          vm.query.make = null
          vm.query.model = null
        }
        if(type === 'model') {
          vm.query.model = null
        }
        await fetchData()
        updateView()
        modalsService.toast(`${type} deleted successfully`)
      }
      else {
        modalsService.toast('An error has ocurred')
        console.log(res.e)
      }
    }

    function updateView() {
      $timeout(function() {
        $scope.$digest()
      })
    }
  } 
}

databaseCtrl.$inject = ['databaseService','$scope','$timeout','modalsService']

export default angular.module('database.controller', [])
  .controller('databaseCtrl', databaseCtrl)
  .name