import databaseService from './database.service'
import databaseCtrl from './database.controller'

export default angular.module('database', [databaseCtrl,databaseService])
  .name