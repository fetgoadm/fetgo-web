class insuranceCtrl { 
  constructor(insuranceService,$scope,$timeout,$rootScope) {
    const vm = this
    vm.setType = setType
    vm.setService = setService
    vm.getData = getData
    vm.setEditableRow = setEditableRow
    vm.updateRow = updateRow
    vm.resetEditable = resetEditable
    vm.deleteModal = deleteModal
    $rootScope.query = {
      type: null,
      service: null
    }

    function getData() {
      insuranceService.getData($rootScope.query, res => {
        vm.data = res
        vm.rows = Object.keys(vm.data).length
        updateView()
      })
    }

    function setType(type) {
      $rootScope.query.type = type ? type : Object.keys($rootScope.types)[0]
      setService()
    }

    function setService(service) {
      $rootScope.query.service = service ? service : Object.keys($rootScope.types[$rootScope.query.type].types)[0]
    }

    async function getInsuranceTypes() {
      let res = await insuranceService.getInsuranceTypes()
      $rootScope.types = res.data
    }

    function setEditableRow(key, row) {
      vm.editable = angular.copy(row)
      vm.editable.id = key
    }

    function resetEditable() {
      delete vm.editable
      updateView()
    }

    async function updateRow() {
      let res = await insuranceService.updateRow($rootScope.query, vm.editable)
      if(res.success) {
        alert('Success')
        resetEditable()
      }
      else {
        alert(res.e)
      }
    }

    function deleteModal(key, item) {
      $rootScope.activeModalData = angular.copy(item)
      $rootScope.activeModalData.id = key
      $rootScope.modalType = 12
    }

    function updateView() {
      $timeout(function() {
        $scope.$digest()
      })
    }

    async function init() {
      await getInsuranceTypes()
      setType()
      setService()
      getData()
    }

    $scope.$on('$destroy', () => {
      delete $rootScope.query
      delete $rootScope.types
    })

    init()
  } 
}

insuranceCtrl.$inject = ['insuranceService','$scope','$timeout','$rootScope']

export default angular.module('insurance.controller', [])
  .controller('insuranceCtrl', insuranceCtrl)
  .name