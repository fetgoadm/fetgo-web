import modalsService from '../modals/modals.service'

class insuranceService {
  constructor(modalsService) {
    this.modal = modalsService
  }

  query = {}

  getData(query, callback) {
    this.query = query
    const ref = firebase.database().ref(query.type).child(query.service)
    ref.on('value', snap => {
      callback(snap.val())
    })
  }

  async getInsuranceTypes() {
    const ref = firebase.database().ref('insurance_types')
    try {
      let snap = await ref.once('value')
      return {success:true, data: snap.val()}
    }
    catch (e) {
      return {success:false, e}
    }
  }

  async updateRow(query, row) {
    const ref = firebase.database().ref(query.type).child(query.service).child(row.id)
    try {
      await ref.update(row)
      return {success:true}
    }
    catch (e) {
      return {success:false, e}
    }
  }
}
  
insuranceService.$inject = ['modalsService']

export default angular.module('insurance.service', [])
  .service('insuranceService', insuranceService)
  .name