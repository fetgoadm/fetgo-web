import insuranceService from './insurance.service'
import insuranceCtrl from './insurance.controller'

export default angular.module('insurance', [insuranceCtrl,insuranceService])
  .name