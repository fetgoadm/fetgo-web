insuranceRoutes.$inject = ['$stateProvider']

function insuranceRoutes($stateProvider) {
  $stateProvider
    .state('insurance', {
      url: '/insurance',
      views: {
        'content': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('./insurance.html')))
            })
          }],
          controller: 'insuranceCtrl',
          controllerAs: 'ins'
        },
        'menu': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('../menu/menu.html')))
            })
          }],
          controller: 'menuCtrl',
          controllerAs: 'menu'
        },
        'modals': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('../modals/modals.html')))
            })
          }],
          controller: 'modalsCtrl',
          controllerAs: 'modals'
        }
      },
      resolve: {
        'currentAuth': ['authService', authService => {
          return authService.check()
        }],
        'loadinsuranceController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('./index')
              $ocLazyLoad.load({name: 'insurance'})
              resolve(module.controller)
            })
          })
        }],
        'loadMenuController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('../menu/index')
              $ocLazyLoad.load({name: 'menu'})
              resolve(module.controller)
            })
          })
        }],
        'loadModalsController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('../modals/index')
              $ocLazyLoad.load({name: 'modals'})
              resolve(module.controller)
            })
          })
        }]
      }
    })
}

export default angular.module('insurance.routing', [])
  .config(insuranceRoutes)
  .name