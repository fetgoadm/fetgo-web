class salesCtrl { 
  constructor(salesService,$state,$timeout,$scope) {
    let vm = this
    vm.showModal = showModal
    vm.toggleFilter = toggleFilter
    vm.resetFilter = resetFilter
    vm.applyFilter = applyFilter
    vm.filters = {
      minDate: null,
      maxDate: null,
      keyword: null,
      limit: 15,
      active: false
    }

    function getSales(opts) {
      salesService.getSales(opts, res => {
        vm.sales = res
        $timeout(() => {
          $scope.$digest()
        })
      })
    }

    function showModal(sale) {
      salesService.showModal(5,sale)
    }

    function toggleFilter() {
      vm.error = false
      vm.filters.minDate = null
      vm.filters.maxDate = null
      vm.filters.active = !vm.filters.active
    }

    function applyFilter() {
      if(vm.filters.minDate && vm.filters.maxDate) {
        vm.error = false
        getSales(vm.filters)
      }
      else vm.error = true
    }

    function resetFilter() {
      vm.error = false
      vm.filters.minDate = null
      vm.filters.maxDate = null
      vm.filters.active = !vm.filters.active
      getSales(vm.filters)
    }

    function getToday() {
      const date = new Date()
      let dd = date.getDate()
      if(dd < 10) {dd = '0' + dd}
      let mm = date.getMonth()+1
      if(mm < 10) {mm = '0' + mm}
      let yyyy = date.getFullYear()
      vm.today = yyyy + '-' + mm + '-' + dd
    }

    const element = document.querySelector('.scrollable')

    element.onscroll = ev => {
      if (element.scrollHeight - element.scrollTop === element.clientHeight) {
        vm.filters.limit += 15
        getSales(vm.filters)
      }
    }

    getSales(vm.filters)
    getToday()
  }
}

salesCtrl.$inject = ['salesService','$state','$timeout','$scope']

export default angular.module('sales.controller', [])
  .controller('salesCtrl', salesCtrl)
  .name