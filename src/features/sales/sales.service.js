import modalsService from '../modals/modals.service'
import homeService from '../home/home.service'

export default class salesService {
  constructor(modalsService,homeService) {
    this.modal = modalsService
    this.home = homeService
  }

  getSales(options, callback) {
    const ref = options.active
      ? firebase.database().ref('sells').orderByChild('date').startAt(options.minDate).endAt(options.maxDate)
      : firebase.database().ref('sells').limitToLast(options.limit)
    ref.on('value', snap => {
      let data = snap.val()
      let sales = []
      for(let id in data) {
        let sale = data[id]
        if(sale.status !== 'rejected') {
          sale.temp = {
            date: this.home.formatDate(sale.date),
            time: this.home.formatTime(sale.time)
          }
          sales.push(sale)
        }
      }
      callback(sales)
    })
  }
  
  showModal(type,sale) {
    this.modal.showModal(type,sale)
  }
}
  
salesService.$inject = ['modalsService','homeService']