import salesService from './sales.service'
import salesCtrl from './sales.controller'
import homeService from '../home/home.service'

export default angular.module('sales', [salesCtrl,homeService])
  .service('salesService', salesService)
  .name