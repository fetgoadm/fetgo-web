import chatService from './chat.service'
import chatCtrl from './chat.controller'

export default angular.module('chat', [chatCtrl, chatService])
  .name