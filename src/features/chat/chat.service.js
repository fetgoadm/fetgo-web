import menuService from '../menu/menu.service'
import moment from 'moment-timezone'

class chatService {
  constructor($firebaseArray, $firebaseObject, menuService, $rootScope) {
    this.array = $firebaseArray
    this.obj = $firebaseObject
    this.menu = menuService
    this.root = $rootScope
  }

  userData = {}

  getUsers(callback) {
    const ref = firebase.database().ref('users')
    const data = this.array(ref)
    callback(data)
  }

  async getActiveRooms(callback) {
    const userData = await this.menu.getUser()
    const ref = firebase.database().ref('users').child(userData.uid).child('rooms')
    const data = this.array(ref)
    callback(data)
  }

  getSelectedRoom(roomId, callback) {
    this.setRoomAsRead(roomId)
    const ref = firebase.database().ref('chat').child(roomId)
    const data = this.obj(ref)
    callback(data)
  }

  setRoomAsRead(roomId) {
    const { uid } = this.root.currentUser
    const ref = firebase.database().ref('users').child(uid).child('rooms').child(roomId)
    ref.update({unread: false})
  }

  async sendMessage(roomId, clientInfo, numChildren, message) {
    if(!roomId) {
      const res = await this.createNewRoom(clientInfo)
      roomId = res.roomId
    }
    const ref = firebase.database().ref('chat').child(roomId).child('messages')
    const clientRef = firebase.database().ref('users').child(clientInfo.uid)
    const clientSnap = await clientRef.once('value')
    const data = clientSnap.val()
    const key = ref.push().key
    const { name, lastName, fcm_token, userPhoto } = this.root.currentUser.data
    const newMessage = {
      text: message,
      checkSum: false,
      date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
      time: moment().tz('asia/dubai').format('hh:mm:ss a'),
      sort: numChildren + 1,
      uid: key,
      from: {
        name,
        lastName,
        fcm_token,
        photo: userPhoto || null
      },
      to: {
        ...clientInfo,
        photo: data.userPhoto,
        fcm_token: data.fcm_token
      }
    }
    try {
      Promise.all([
        await ref.child(key).set(newMessage),
        await clientRef.child('rooms').child(roomId).update({unread: true}),
        await this.setRoomAsRead(roomId)
      ])
      return {success: true, roomId}
    }
    catch(e) {
      alert('error')
      console.log(e)
    }
  }

  async createNewRoom(clientInfo) {
    const currentUser = {...this.root.currentUser}
    const client = {
      fcm_token: clientInfo.fcm_token,
      email: clientInfo.email,
      uid: clientInfo.uid,
      name: clientInfo.name || null,
      lastName: clientInfo.lastName || null,
      displayName: clientInfo.displayName || null,
      phone: clientInfo.phone
    }
    const operator = {
      uid: currentUser.uid,
      name: currentUser.data.name,
      lastName: currentUser.data.lastName,
      email: currentUser.data.email
    }
    const chatRef = firebase.database().ref('chat')
    const key = chatRef.push().key
    const clientRef = firebase.database().ref('users').child(client.uid).child('rooms').child(key)
    const operatorRef = firebase.database().ref('users').child(operator.uid).child('rooms').child(key)
    const chatRoom = {
      uid: key,
      client,
      operator,
      o_flag: true,
    }
    const userRoom = {
      date: moment().tz('asia/dubai').format('YYYY-MM-DD'),
      time: moment().tz('asia/dubai').format('hh:mm:ss a'),
      uid: key,
      client: `${client.name} ${client.lastName}` || client.displayName,
      clientuid: client.uid,
      operator: `${operator.name} ${operator.lastName}`,
      operatoruid: operator.uid,
    }

    try {
      await Promise.all([
        chatRef.child(key).set(chatRoom),
        clientRef.set(userRoom),
        operatorRef.set(userRoom)
      ])

      return {success:true, roomId: key}
    }
    catch(e) {
      return {success:false, e}
    }
  }
}
  
chatService.$inject = ['$firebaseArray', '$firebaseObject', 'menuService', '$rootScope']

export default angular.module('chat.service', [])
  .service('chatService', chatService)
  .name