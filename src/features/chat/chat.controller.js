import moment from 'moment-timezone'
import { formatTitle } from '../home/home.service'
class chatCtrl { 
  constructor(chatService,$state,$timeout,$scope,$rootScope) {
    let vm = this
    vm.formatDate = formatDate
    vm.getSelectedRoom = getSelectedRoom
    vm.sendMessage = sendMessage
    vm.closeRoom = closeRoom
    vm.toggleOnlineUsers = toggleOnlineUsers
    vm.viewOnlineUsers = false
    vm.formatTitle = formatTitle
    vm.newRoom = newRoom
    vm.closeRoomModal = closeRoomModal
    
    chatService.getUsers(res => {
      vm.users = res
    })

    chatService.getActiveRooms(res => {
      vm.activeRooms = res
    })

    function checkRedirect() {
      if($rootScope.roomId) {
        getSelectedRoom($rootScope.roomId)
        delete $rootScope.roomId
      }
    }

    function updateView() {
      $timeout(function() {
        $scope.$digest()
      })
    }

    function formatDate(date) {
      return moment(date).format('L')
    }

    function getSelectedRoom(roomId) {
      chatService.getSelectedRoom(roomId, res => {
        $rootScope.selectedRoom = res
        $timeout(scrollBottom, 100)
      })
    }

    async function sendMessage() {
      const numChildren = $rootScope.selectedRoom.messages ? Object.keys($rootScope.selectedRoom.messages).length : 0
      const msg = vm.agentMessage
      vm.agentMessage = ''
      const res = await chatService.sendMessage($rootScope.selectedRoom.uid || null, $rootScope.selectedRoom.client, numChildren, msg)
      if(res.success) {
        scrollBottom()
        updateView()
        if(vm.viewOnlineUsers) {
          vm.viewOnlineUsers = false
          delete $rootScope.selectedRoom
          getSelectedRoom(res.roomId)
        }
      } else {
        vm.agentMessage = msg
        console.log(res.e)
      }
    }

    function scrollBottom() {
      const el = document.querySelector('.chatbox__messages')
      el.scrollTop = el.scrollHeight
    }

    async function closeRoom() {
      const { client: {uid: clientId}, operator: {uid: agentId}, uid: roomId } = $rootScope.selectedRoom
    }

    function toggleOnlineUsers() {
      vm.viewOnlineUsers = !vm.viewOnlineUsers
      delete $rootScope.selectedRoom
    }

    function newRoom(client) {
      $rootScope.selectedRoom = {
        client: {
          fcm_token: client.fcm_token,
          name: client.name || null,
          lastName: client.lastName || null,
          displayName: client.displayName || null,
          uid: client.$id,
          phone: client.phone,
          email: client.email
        }
      }
    }

    function closeRoomModal() {
      $rootScope.modalType = 16
      $rootScope.activeModalData = {...$rootScope.selectedRoom}
      delete $rootScope.selectedRoom
    }

    checkRedirect()
  }
}

chatCtrl.$inject = ['chatService','$state','$timeout','$scope','$rootScope']

export default angular.module('chat.controller', [])
  .controller('chatCtrl', chatCtrl)
  .name