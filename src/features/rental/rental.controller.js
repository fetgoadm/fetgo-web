class rentalCtrl { 
  constructor(rentalService,$state) {
    let vm = this
    vm.getItems = getItems
    vm.toggleSearch = toggleSearch
    vm.showModal = showModal
    vm.showConfirm = showConfirm

    function getItems(type) {
      vm.active = type
      rentalService.getItems(type, res => {
        vm.items = res
      })
    }

    function toggleSearch() {
      vm.keyword = ''
      vm.filter = !vm.filter
      if(!vm.filter) getItems(vm.active)
    }

    function showModal(obj) {
      obj.type = vm.active
      rentalService.showModal(obj)
    }

    function showConfirm(obj) {
      rentalService.showConfirm(obj)
    }

    getItems('luxury')
  }
}

rentalCtrl.$inject = ['rentalService','$state']

export default angular.module('rental.controller', [])
  .controller('rentalCtrl', rentalCtrl)
  .name