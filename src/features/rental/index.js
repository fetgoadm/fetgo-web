import rentalService from './rental.service'
import rentalCtrl from './rental.controller'

export default angular.module('rental', [rentalCtrl, rentalService])
  .name