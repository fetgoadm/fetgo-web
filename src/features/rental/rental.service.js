import modalsService from '../modals/modals.service'

class rentalService {
  constructor($firebaseArray,modalsService) {
    this.modal = modalsService
    this.array = $firebaseArray
  }

  getItems(type, callback) {
    const ref = firebase.database().ref('car_rental').child(type).orderByChild('name')
    const data = this.array(ref)
    callback(data)
  }

  showModal(obj) {
    this.modal.showModal(13,obj)
  }
  

  showConfirm(obj) {
    this.modal.showModal(12, obj)
  }
}
  
rentalService.$inject = ['$firebaseArray','modalsService']

export default angular.module('rental.service', [])
  .service('rentalService', rentalService)
  .name