import menuService from './menu.service'
import menuCtrl from './menu.controller'

export default angular.module('menu', [menuCtrl])
  .service('menuService', menuService)
  .name