class menuCtrl { 
  constructor(authService,menuService,$scope,$state,$timeout,$window,$rootScope) {
    let vm = this
    let root = $rootScope
    vm.signOut = signOut
    vm.add = add
    vm.currentState = $state.current.name
    vm.state = $state

    function signOut() {
      authService.signOut()
      $state.go('login')
    }

    async function getUser() {
      if(!root.currentUser) {
        let res = await menuService.getUser()
        root.currentUser = res
        updateView()
      }
    }

    async function getUserTypes() {
      if(!root.userTypes) {
        let res = await menuService.getUserTypes()
        root.userTypes = res
      }
    }

    function add() {
      switch($state.current.name) {
        case 'users':
          menuService.showModal(1)
          break
        case 'tuning':
          menuService.showModal(11)
          break
        case 'rental':
          menuService.showModal(13)
          break
        case 'insurance':
          menuService.showModal(15)
          break
      }
    }

    function updateView() {
      $timeout(function() {
        $scope.$digest()
      })
    }

    function getUnreadChatFlag() {
      menuService.getUnreadChatFlag(res => {
        updateView()
      })
    }

    getUnreadChatFlag()
    getUser()
    getUserTypes()
  } 
}

menuCtrl.$inject = ['authService','menuService','$scope','$state','$timeout','$window','$rootScope']

export default angular.module('menu.controller', [])
  .controller('menuCtrl', menuCtrl)
  .name