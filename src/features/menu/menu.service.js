import modalsService from '../modals/modals.service'
import soundFile from '../../assets/sounds/chat.mp3'
export default class menuService {
  constructor($firebaseAuth,$firebaseObject,$firebaseArray,modalsService,$rootScope) {
    this.auth = $firebaseAuth
    this.object = $firebaseObject
    this.array = $firebaseArray
    this.modal = modalsService
    this.root = $rootScope
  }

  userData = {}
  
  async getUser() {
    if(Object.keys(this.userData)) {
      const uid = this.auth().$getAuth().uid
      const ref = firebase.database().ref('users').child(uid)
      const snap = await ref.once('value')
      const data = snap.val()
      this.userData = {uid, data}
    }
    return this.userData
  }

  getUserTypes() {
    const ref = firebase.database().ref('user_types')
    const data = this.object(ref)
    return data
  }

  showModal(type) {
    this.modal.showModal(type)
  }

  async getUnreadChatFlag(callback) {
    const { uid } = await this.getUser()
    const ref = firebase.database().ref('users').child(uid).child('rooms')
    ref.on('value', snap => {
      if(snap.exists()) {
        const data = snap.val()
        const currentUnread = this.root.unread
        this.root.unread = Object.values(data).findIndex(el => el.unread) > -1
        currentUnread !== this.root.unread && this.root.unread && new Audio(soundFile).play()
        callback('updated')
      }
      else {
        callback('updated')
      }
    })
  }
}
  
menuService.$inject = ['$firebaseAuth','$firebaseObject','$firebaseArray','modalsService','$rootScope']