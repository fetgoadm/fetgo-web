import historyService from './history.service'
import historyCtrl from './history.controller'
import homeService from '../home/home.service'

export default angular.module('history', [historyCtrl,homeService])
  .service('historyService', historyService)
  .name