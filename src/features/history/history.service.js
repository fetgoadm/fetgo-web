import moment from 'moment-timezone'
import modalsService from '../modals/modals.service'

export default class historyService {
  constructor(homeService,$rootScope,modalsService) {
    this.home = homeService
    this.modal = modalsService
    this.root = $rootScope
  }

  getTicketHistory(options,callback) {
    const ref = options.active
      ? firebase.database().ref('history_orders').orderByChild('date').startAt(options.minDate).endAt(options.maxDate)
      : firebase.database().ref('history_orders').limitToLast(options.limit)
    ref.on('value', snap => {
      let ticketHistory = []
      let data = snap.val()

      /* Feed global ticket lists for Admin type users
        and user specific lists for Agent type users */
      this.root.currentUser.data.user_type === 'ut-001'
        ? (() => {
          for(let order in data) {
            let ticket = this.home.formatTicketData(data[order])
            ticketHistory.push(ticket)
          }
        })()
        : (() => {
          for(let order in data) {
            data[order].attended_by === this.root.currentUser.uid
              ? (() => {
                let ticket = this.home.formatTicketData(data[order])
                ticketHistory.push(ticket)
              })()
              : null
          }
        })()

      callback(ticketHistory)
    })
  }

  showModal(ticket) {
    this.modal.showModal(0,ticket)
  }
}
  
historyService.$inject = ['homeService','$rootScope','modalsService']