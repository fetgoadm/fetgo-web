class usersCtrl { 
  constructor(usersService,$http,$state,$stateParams) {
    let vm = this
    vm.getUsers = getUsers
    vm.showModal = showModal
    vm.toggleSearch = toggleSearch
    vm.filter = false

    function getUsers(type) {
      vm.active = type
      usersService.getUsers(type, res => {
        vm.users = res.data
      })
    }

    function showModal(type,obj) {
      const data = {...obj}
      usersService.showModal(type,data)
    }

    function toggleSearch() {
      vm.keyword = ''
      vm.filter = !vm.filter
      getUsers(vm.active)
    }
    getUsers($state.params.type || 'App User')
  }
}

usersCtrl.$inject = ['usersService','$http','$state','$stateParams']

export default angular.module('users.controller', [])
  .controller('usersCtrl', usersCtrl)
  .name