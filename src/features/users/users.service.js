import modalsService from '../modals/modals.service'

class usersService {
  constructor($firebaseArray,modalsService) {
    this.modal = modalsService
    this.array = $firebaseArray
  }

  userTypes = {
    'Admin': 'ut-001',
    'Agent': 'ut-002',
    'App User': 'ut-003'
  }
  
  getUsers(type,callback) {
    const userType = this.userTypes[type]
    const ref = firebase.database().ref('users').orderByChild('user_type').equalTo(userType)
    const data = this.array(ref)
    callback({data})
  }
  
  showModal(type,user) {
    this.modal.showModal(type,user)
  }
}
  
usersService.$inject = ['$firebaseArray','modalsService']

export default angular.module('users.service', [])
  .service('usersService', usersService)
  .name