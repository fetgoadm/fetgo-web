import usersService from './users.service'
import usersCtrl from './users.controller'

export default angular.module('users', [usersCtrl, usersService])
  .name