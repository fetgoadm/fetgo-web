import usersService from '../users/users.service'
import modalsService from '../modals/modals.service'

class pushCtrl { 
  constructor(modalsService,usersService,$state,$timeout,$scope,$rootScope) {
    let vm = this
    vm.sendNotification = sendNotification
    vm.updateUserList = updateUserList
    vm.toggleAll = toggleAll
    vm.showAttachmentModal = showAttachmentModal
    vm.selectedUsers = []
    vm.selectAll = false
    vm.loading = false
    
    function getUsers() {
      const type = 'App User'
      usersService.getUsers(type, res => {
        vm.users = res.data
      })
    }

    function updateUserList() {
      const currentUserArray = [...vm.selectedUsers]
      vm.recipients = currentUserArray
        .map(user => {
          let name = user.displayName || `${user.name} ${user.lastName}`
          return name
        })
        .join(', ')
    }

    async function sendNotification() {
      loading(true)
      const currentUserArray = [...vm.selectedUsers]
      const userData = currentUserArray
        .map(user => {
          return {
            id: user.$id,
            name: user.name || user.displayName.split(' ').shift(),
            lastName: user.lastName || user.displayName.split(' ').pop(),
            fullName: user.displayName || `${user.name} ${user.lastName}`,
            email: user.email || 'N/A'
          }
        })
      let res = await modalsService.sendNotification(vm.selectAll,userData,vm.pushContent)
      loading(false)
      if(res.success) {
        delete $rootScope.activeModalData
        toast('Push notification sent')
        resetPage()
      }
      else {
        toast('An error has occured. Please try again.')
      }
      updateView()
    }

    function toast(text) {
      updateView()
      modalsService.toast(text)
    }

    function showAttachmentModal() {
      updateView()
      modalsService.showModal(8)
    }

    function toggleAll() {
      vm.keyword = ''
      vm.selectAll = !vm.selectAll
      vm.selectAll ? vm.selectedUsers = [...vm.users] : vm.selectedUsers = []
      updateUserList()
    }

    function updateView() {
      $timeout(() => {
        $scope.$digest()
      })
    }

    function loading(status) {
      vm.loading = status
    }

    function resetPage() {
      vm.selectedUsers = []
      vm.pushContent = ''
      vm.selectAll = false
      updateUserList()
    }

    getUsers()
  }
}

pushCtrl.$inject = ['modalsService','usersService','$state','$timeout','$scope','$rootScope']

export default angular.module('push.controller', [])
  .controller('pushCtrl', pushCtrl)
  .name