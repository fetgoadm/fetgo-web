import pushCtrl from './push.controller'
import usersService from '../users/users.service'

export default angular.module('push', [pushCtrl, usersService])
  .name