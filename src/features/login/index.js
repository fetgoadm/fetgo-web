import loginCtrl from './login.controller'

export default angular.module('login', [loginCtrl])
  .name