class loginCtrl { 
  constructor(authService,$state,$scope,$timeout) {
    let vm = this
    vm.signIn = signIn
    vm.reset = reset
    vm.year = new Date().getFullYear()
    
    async function signIn() {
      let btn = angular.element(document.querySelector('#btn'))
      if(vm.email && vm.password) {
        btn.addClass('disabled')
        let res = await authService.signIn(vm.email,vm.password)
        if(res.success) {
          $state.go('home')
        }
        else {
          btn.removeClass('disabled')
          vm.errorMsg = res.msg
          $timeout(() => {
            $scope.$digest() 
          })
        }
      }
      else {
        if(!vm.email) {
          let elements = angular.element(document.querySelectorAll('#email,#icon-email'))
          elements.addClass('error')
        }
        if(!vm.password) {
          let elements = angular.element(document.querySelectorAll('#password,#icon-password'))
          elements.addClass('error')
        }
      }
    }

    function reset() {
      let elements = angular.element(document.querySelectorAll('#email,#password,#icon-email,#icon-password'))
      elements.removeClass('error')
    }
  }
}

loginCtrl.$inject = ['authService','$state','$scope','$timeout']

export default angular.module('login.controller', [])
  .controller('loginCtrl', loginCtrl)
  .name