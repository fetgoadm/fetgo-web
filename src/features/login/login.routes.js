loginRoutes.$inject = ['$stateProvider']

function loginRoutes($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/login',
      views: {
        'content': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              require.ensure([], () => resolve(require('./login.html')))
            })
          }],
          controller: 'loginCtrl',
          controllerAs: 'login'
        }
      },
      resolve: {
        'loadLoginController': ['$q', '$ocLazyLoad', ($q, $ocLazyLoad) => {
          return $q((resolve) => {
            require.ensure([], () => {
              let module = require('./index')
              $ocLazyLoad.load({name: 'login'})
              resolve(module.controller)
            })
          })
        }]
      }
    })
}

export default angular.module('login.routing', [])
  .config(loginRoutes)
  .name