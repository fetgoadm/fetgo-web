if (module.hot) {
  module.hot.accept()
}

import uirouter from '@uirouter/angularjs'
import firebase from 'angularfire'
import oclazyload from 'oclazyload'
import routing from './app.config'
import auth from './features/auth'
import loginRoutes from './features/login/login.routes'
import homeRoutes from './features/home/home.routes'
import historyRoutes from './features/history/history.routes'
import usersRoutes from './features/users/users.routes'
import salesRoutes from './features/sales/sales.routes'
import pushRoutes from './features/push/push.routes'
import pushHistoryRoutes from './features/pushHistory/pushHistory.routes'
import tuningRoutes from './features/tuning/tuning.routes'
import rentalRoutes from './features/rental/rental.routes'
import databaseRoutes from './features/database/database.routes'
import insuranceRoutes from './features/insurance/insurance.routes'
import chatRoutes from './features/chat/chat.routes'
const checklistModel = require('checklist-model')

import './style.scss'

angular.module('app', [
  uirouter,
  firebase,
  oclazyload,
  auth,
  loginRoutes,
  homeRoutes,
  historyRoutes,
  usersRoutes,
  salesRoutes,
  pushRoutes,
  pushHistoryRoutes,
  tuningRoutes,
  rentalRoutes,
  databaseRoutes,
  insuranceRoutes,
  chatRoutes,
  checklistModel
])
  .config(routing)

if('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('./dist/sw.js')
  })
}