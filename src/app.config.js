routing.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider']

export default function routing($urlRouterProvider, $locationProvider, $stateProvider) {
  $stateProvider
    .state('app', {
      url: '/',
      onEnter: ['$state','authService', ($state,authService) => {
        authService.check() ? $state.go('home') : $state.go('login')
      }]
    })

  $urlRouterProvider.otherwise('/')
  $locationProvider.html5Mode(true)
}