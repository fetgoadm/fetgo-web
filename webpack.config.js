const webpack = require('webpack')
const path = require('path')
const workboxPlugin = require('workbox-webpack-plugin')

const config = {
  context: path.resolve(__dirname, 'src'),
  entry: ['babel-polyfill','./app.js'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/dist/',
    filename: 'bundle.js'
  },
  node: {
    fs: 'empty'
  },
  module: {
    rules:[{
      test: /\.js$/,
      include: path.resolve(__dirname, 'src'),
      use: [{
        loader: 'babel-loader',
        query: {
          plugins: ['transform-runtime'],
          presets: ['es2015', 'stage-0']
        }
      }]
    }, {
      test: /\.(scss|css)$/,
      loader: ['style-loader', 'css-loader','sass-loader']
    }, {
      test: /\.html$/,
      loader: 'html-loader'
    }, {
      test: /\.(png|gif|jpg|woff|woff2|eot|ttf|svg|mp3)$/,
      loader: 'file-loader',
      query: {
        name: 'static/media/[name].[ext]'
      }
    }]
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new workboxPlugin({
      globDirectory: './dist/',
      globPatterns: ['**/*.{html,js,mp3,jpg,woff2,svg}'],
      swDest: path.join('./dist/', 'sw.js'),
      clientsClaim: true,
      skipWaiting: true,
      runtimeCaching: [
        {
          urlPattern: new RegExp('https://fetgo-203d8.firebaseio.com'),
          handler: 'staleWhileRevalidate'
        }
      ]
    })
  ]
}

module.exports = config